﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWarBehaviour : MonoBehaviour
{

    public GameObject fowPlane;

    public GameObject player;

    public GameObject market;

    public GameObject potion;

    public LayerMask fogMask;

    public float fogRadius;

    private float fogRadiusSqr => Mathf.Pow(fogRadius, 2);

    private Mesh planeMesh;

    private Vector3[] meshVertices;

    private Color[] meshColors;

    private RaycastHit playerHit;

    private RaycastHit marketHit;

    private RaycastHit potionHit;

    private PlayerMovement playerMovement;

    private Ray playerRay => new Ray(transform.position, player.transform.position - transform.position);

    private Ray marketRay => new Ray(transform.position, market.transform.position - transform.position);

    private Ray potionRay => new Ray(transform.position, potion.transform.position - transform.position);


    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {

        if (Physics.Raycast(playerRay, out playerHit, 1000, fogMask, QueryTriggerInteraction.Collide))
        {
            for (int i = 0; i < meshVertices.Length; i++)
            {
                Vector3 transformedPositionPlayer = fowPlane.transform.TransformPoint(meshVertices[i]);

                float pDistance = Vector3.SqrMagnitude(transformedPositionPlayer - playerHit.point);

                if (pDistance < fogRadius)
                {
                    float alpha = Mathf.Min(meshColors[i].a, pDistance / fogRadiusSqr);

                    meshColors[i].a = alpha;
                }
            }
            UpdateMeshColor();
        }

        if (playerMovement.moving == false)
        {

            if (Physics.Raycast(marketRay, out marketHit, 1000, fogMask, QueryTriggerInteraction.Collide))
            {
                for (int i = 0; i < meshVertices.Length; i++)
                {
                    Vector3 transformedPositionMarket = fowPlane.transform.TransformPoint(meshVertices[i]);

                    float mDistance = Vector3.SqrMagnitude(transformedPositionMarket - marketHit.point);

                    if (mDistance < fogRadius)
                    {
                        float alpha = Mathf.Min(meshColors[i].a, mDistance / fogRadiusSqr);

                        meshColors[i].a = alpha;
                    }
                }
                UpdateMeshColor();
            }

            if (Physics.Raycast(potionRay, out potionHit, 1000, fogMask, QueryTriggerInteraction.Collide))
            {
                for (int i = 0; i < meshVertices.Length; i++)
                {
                    Vector3 transformedPositionPotion = fowPlane.transform.TransformPoint(meshVertices[i]);

                    float pDistance = Vector3.SqrMagnitude(transformedPositionPotion - potionHit.point);

                    if (pDistance < fogRadius)
                    {
                        float alpha = Mathf.Min(meshColors[i].a, pDistance / fogRadiusSqr);

                        meshColors[i].a = alpha;
                    }
                }
                UpdateMeshColor();
            }
        }
    }


    private void Initialize()
    {

        playerMovement = player.GetComponent<PlayerMovement>();

        planeMesh = fowPlane.GetComponent<MeshFilter>().mesh;

        meshVertices = planeMesh.vertices;

        meshColors = new Color[meshVertices.Length];

        for(int i = 0; i < meshColors.Length; i++)
        {
            meshColors[i] = Color.black;
        }

        UpdateMeshColor();
    }

    private void UpdateMeshColor()
    {
        planeMesh.colors = meshColors;
    }
}
