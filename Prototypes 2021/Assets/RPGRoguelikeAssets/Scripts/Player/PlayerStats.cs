﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public bool hideStartingStats;

    [DrawIf("hideStartingStats", true, DisablingType.DontDraw)]
    public int startingHealth;
    public FloatVariable maxHealth;
    public FloatVariable health;

    [DrawIf("hideStartingStats", true, DisablingType.DontDraw)]
    public int startingConstitution;
    public FloatVariable constitution;

    [DrawIf("hideStartingStats", true, DisablingType.DontDraw)]
    public int startingStrenght;
    public FloatVariable strenght;

    [DrawIf("hideStartingStats", true, DisablingType.DontDraw)]
    public int startingDexterity;
    public FloatVariable dexterity;

    [DrawIf("hideStartingStats", true, DisablingType.DontDraw)]
    public int startingReflex;
    public FloatVariable reflex;

    [DrawIf("hideStartingStats", true, DisablingType.DontDraw)]
    public int startingArmor;
    public FloatVariable armor;
}
