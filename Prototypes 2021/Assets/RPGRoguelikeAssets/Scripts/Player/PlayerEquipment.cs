﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerEquipment : MonoBehaviour
{
    public bool hideEquip;
    public bool hideStats;

	#region Variables
	[DrawIf("hideEquip", false, DisablingType.DontDraw)]
    public ArmorStats helm;
    [DrawIf("hideEquip", false, DisablingType.DontDraw)]
    public ArmorStats chest;
    [DrawIf("hideEquip", false, DisablingType.DontDraw)]
    public ArmorStats legs;
    [DrawIf("hideEquip", false, DisablingType.DontDraw)]
    public ArmorStats boots;

    [DrawIf("hideEquip", false, DisablingType.DontDraw)]
    public WeaponStats sword;
    [DrawIf("hideEquip", false, DisablingType.DontDraw)]
    public WeaponStats hammer;
    [DrawIf("hideEquip", false, DisablingType.DontDraw)]
    public WeaponStats bow;
    [DrawIf("hideEquip", false, DisablingType.DontDraw)]
    public WeaponStats dagger;
    [DrawIf("hideEquip", false, DisablingType.DontDraw)]
    public WeaponStats special;

    [DrawIf("hideEquip", false, DisablingType.DontDraw)]
    public WeaponStats currentWeapon;

    [DrawIf("hideStats", false, DisablingType.DontDraw)]
    public FloatVariable constitution;
    [DrawIf("hideStats", false, DisablingType.DontDraw)]
    public FloatVariable strenght;
    [DrawIf("hideStats", false, DisablingType.DontDraw)]
    public FloatVariable dexterity;
    [DrawIf("hideStats", false, DisablingType.DontDraw)]
    public FloatVariable reflex;
    [DrawIf("hideStats", false, DisablingType.DontDraw)]
    public FloatVariable armor;
    [DrawIf("hideStats", false, DisablingType.DontDraw)]
    public FloatVariable damage;

	#endregion

	public static Action<WeaponStats> ShowEquippedWeapon;
    public static Action<ArmorStats> ShowEquippedArmor;

    private void Awake()
    {
        ItemSlotBehaviour.EquipItem += OnEquipItem;
    }

    public void UpdateArmor(ArmorStats newArmor)
    {
        switch (newArmor.armorType)
        {
            case ArmorStats.ArmorType.Helm:
                constitution.Value += newArmor.constitution - helm.constitution;
                strenght.Value += newArmor.strenght - helm.strenght;
                dexterity.Value += newArmor.dexterity - helm.dexterity;
                reflex.Value += newArmor.reflex - helm.reflex;
                armor.Value += newArmor.armor - helm.armor;
                helm = newArmor;
                break;

            case ArmorStats.ArmorType.Chest:
                constitution.Value += newArmor.constitution - chest.constitution;
                strenght.Value += newArmor.strenght - chest.strenght;
                dexterity.Value += newArmor.dexterity - chest.dexterity;
                reflex.Value += newArmor.reflex - chest.reflex;
                armor.Value += newArmor.armor - chest.armor;
                chest = newArmor;
                break;

            case ArmorStats.ArmorType.Legs:
                constitution.Value += newArmor.constitution - legs.constitution;
                strenght.Value += newArmor.strenght - legs.strenght;
                dexterity.Value += newArmor.dexterity - legs.dexterity;
                reflex.Value += newArmor.reflex - legs.reflex;
                armor.Value += newArmor.armor - legs.armor;
                legs = newArmor;
                break;

            case ArmorStats.ArmorType.Boots:
                constitution.Value += newArmor.constitution - boots.constitution;
                strenght.Value += newArmor.strenght - boots.strenght;
                dexterity.Value += newArmor.dexterity - boots.dexterity;
                reflex.Value += newArmor.reflex - boots.reflex;
                armor.Value += newArmor.armor - boots.armor;
                boots = newArmor;
                break;
        }
    }

    public void UpdateWeapon(WeaponStats newWeapon)
    {
        switch (newWeapon.weaponType)
        {
            case WeaponStats.WeaponType.Slashing:
                sword = newWeapon;                
                break;
            case WeaponStats.WeaponType.Piercing:
                bow = newWeapon;
                break;
            case WeaponStats.WeaponType.Bludgeoning:
                hammer = newWeapon;
                break;
            case WeaponStats.WeaponType.Bleeding:
                dagger = newWeapon;
                break;
            case WeaponStats.WeaponType.Special:
                special = newWeapon;
                break;

        }
        if(currentWeapon.weaponType == newWeapon.weaponType)
        {
            ChangeWeapon(newWeapon);
        }
    }

    public void ChangeWeapon(WeaponStats equippedWeapon)
    {
        constitution.Value += equippedWeapon.constitution - currentWeapon.constitution;
        strenght.Value += equippedWeapon.strenght - currentWeapon.strenght;
        dexterity.Value += equippedWeapon.dexterity - currentWeapon.dexterity;
        reflex.Value += equippedWeapon.reflex - currentWeapon.reflex;
        damage.Value += equippedWeapon.damage - currentWeapon.damage;
        currentWeapon = equippedWeapon;
    }

   public void OnEquipItem(ItemStats newItem)
   {
        WeaponStats newWeapon;

        ArmorStats newArmor;

        if(newItem as ArmorStats != null)
        {
            Debug.Log("Weapon");
        }
        try
        {
            newWeapon = (WeaponStats)newItem;
            UpdateWeapon(newWeapon);
            ShowEquippedWeapon?.Invoke(newWeapon);
        }
        catch (InvalidCastException)
        {
            newArmor = (ArmorStats)newItem;
            UpdateArmor(newArmor);
            ShowEquippedArmor?.Invoke(newArmor);
        }

    }
}
