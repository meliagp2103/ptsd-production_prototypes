﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerButtons : MonoBehaviour
{
    public GameObject weaponOne;
    public GameObject weaponTwo;
    public int currentWeapon;
    public void changeWeapon()
    {

        if(currentWeapon == 0)
        {
            weaponOne.SetActive(false);
            weaponTwo.SetActive(true);
            currentWeapon = 1;
        }
        else
        {
            weaponOne.SetActive(true);
            weaponTwo.SetActive(false);
            currentWeapon = 0;
        }
    }
    public void AttackButton()
    {
        GameManager.instance.ChangeGameState(GameManager.gameState.quickTimeEvent);
    }
}
