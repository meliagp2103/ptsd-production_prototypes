﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField]
    private AnimationCurve lerpCurve;
    private float time;
    public float duration;
    RaycastHit hit;
    public LayerMask layerMask;
    private float distance;
    [SerializeField]
    public bool moving;

   
    void Update()
    {
        switch (GameManager.instance.currentGameState)
        {
            case GameManager.gameState.mapNavigation:
                if (!moving)
                {
                    Movement();
                }
                return;
            default:
                return;
        }
    }
	#region Movement
	private void Movement()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (Physics.Raycast(transform.position, Vector3.forward, out hit, 30f, layerMask))
            {
                moving = true;
                distance = Vector3.Distance(transform.position, hit.transform.position);
                StartCoroutine(MoveToRoom());
            }
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (Physics.Raycast(transform.position, Vector3.left, out hit, 30f, layerMask))
            {
                moving = true;
                distance = Vector3.Distance(transform.position, hit.transform.position);
                StartCoroutine(MoveToRoom());
            }
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            if (Physics.Raycast(transform.position, Vector3.right, out hit, 30f, layerMask))
            {
                moving = true;
                distance = Vector3.Distance(transform.position, hit.transform.position);
                StartCoroutine(MoveToRoom());
            }
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            if (Physics.Raycast(transform.position, Vector3.back, out hit, 30f, layerMask))
            {
                moving = true;
                distance = Vector3.Distance(transform.position, hit.transform.position);
                StartCoroutine(MoveToRoom());
            }
        }
    }
    private IEnumerator MoveToRoom()
    {
        while (time <= duration)
        {
            time += Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, hit.transform.position, distance / duration * Time.deltaTime);
            yield return null;
        }
        moving = false;
        time = 0;
    }
	#endregion
}
