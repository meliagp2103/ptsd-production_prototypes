﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public InventoryBehaviour inventory;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            Enemy enemy = other.GetComponent<Enemy>();
            Instantiate(enemy.combatEnemy, GameManager.instance.combatEnemyPosition.position, Quaternion.identity);
            
            GameManager.instance.ChangeGameState(GameManager.gameState.combat);
            other.gameObject.SetActive(false);
        }

       /* if(other.tag == "Weapon")
        {
            GameManager.instance.swithWeapon.SetActive(true);
            WeaponBehaviour weaponPicked = other.gameObject.GetComponent<WeaponBehaviour>();
            inventory.itemList.Add(weaponPicked);
            //Destroy(other.gameObject);
            other.gameObject.SetActive(false);
        }

        if(other.tag == "Potion")
        {
            GameManager.instance.health += 5;
            PotionBehaviour potionPicked = other.gameObject.GetComponent<PotionBehaviour>();
            inventory.itemList.Add(potionPicked);
            other.gameObject.SetActive(false);
            //Destroy(other.gameObject);
        }

        if(other.tag == "Armor")
        {
            ArmorBehaviour armorPicked = other.gameObject.GetComponent<ArmorBehaviour>();
            inventory.itemList.Add(armorPicked);
            other.gameObject.SetActive(false);
        }*/
    }
}
