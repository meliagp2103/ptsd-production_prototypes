﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class PointsToHit : MonoBehaviour
{
    public void Clicked()
    {
        if(QTEManager.instance.pointsToHit[0] == this.gameObject)
        {
            QTEManager.instance.pointsToHit.Remove(this.gameObject);
            Destroy(this.gameObject);
        }       
    }
    
}
