﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.UI;

public class InventoryBehaviour : MonoBehaviour
{
    private ArmorStats helm;
    public Image activeHelm;

    private ArmorStats chest;
    public Image activeChest;

    private ArmorStats legs;
    public Image activeLegs;

    private ArmorStats boots;
    public Image activeBoots;


    private WeaponStats bow;
    public Image activeBow;

    private WeaponStats dagger;
    public Image activeDagger;

    private WeaponStats hammer;
    public Image activeHammer;

    private WeaponStats special;
    public Image activeSpecial;

    private WeaponStats sword;
    public Image activeSword;



    public List<ItemStats> itemList;
    public List<WeaponStats> weaponList;
    public List<ArmorStats> armorList;

    public Dictionary<Type, List<ItemStats>> inventoryDictionary = new Dictionary<Type, List<ItemStats>>();

    public List<ItemSlotBehaviour> itemsShowedList;
    public Sprite defaultSprite;

    public FloatVariable playerHealth;
    public Text playerHealthText;

    public FloatVariable playerDamage;
    public Text playerDamageText;



    private void Awake()
    {
        PlayerEquipment.ShowEquippedWeapon += DisplayEquippedWeapon;
        PlayerEquipment.ShowEquippedArmor += DisplayEquippedArmor;
        if (inventoryDictionary.ContainsKey(typeof(WeaponStats)))
        {
            inventoryDictionary[typeof(WeaponStats)].Add(weaponList[0]);
        }
        else
        {
            inventoryDictionary.Add(typeof(WeaponStats), new List<ItemStats>() { weaponList[0] });
        }

        if (inventoryDictionary.ContainsKey(typeof(ArmorStats)))
        {
            inventoryDictionary[typeof(ArmorStats)].Add(armorList[0]);
        }
        else
        {
            inventoryDictionary.Add(typeof(ArmorStats), new List<ItemStats>() { armorList[0] });
        }
    }
    private void OnEnable()
    {
        DisplayGear();
    }
    public void UpdateStats()
    {
        playerHealthText.text = playerHealth.Value.ToString();
        playerDamageText.text = playerDamage.Value.ToString();
        DisplayWeapons();
    }

    public void DisplayWeapons()
    {

        foreach(ItemSlotBehaviour itemSlot in itemsShowedList)
        {
            itemSlot.image.sprite = defaultSprite;
            itemSlot.item = null;
        }
        int i = 0;
        foreach(var item in inventoryDictionary[typeof(WeaponStats)])
        {
            itemsShowedList[i].item = item;
            itemsShowedList[i].image.sprite = item.itemSprite;
            i++;
        }


        /*foreach(WeaponStats weaponStats in weaponList)
        {   
            itemsShowedList[i].item = weaponStats;
            itemsShowedList[i].image.sprite = weaponStats.itemSprite;
            i++;
        }*/
    }

    public void DisplayArmors()
    {
        foreach (ItemSlotBehaviour itemSlot in itemsShowedList)
        {
            itemSlot.image.sprite = defaultSprite;
            itemSlot.item = null;
        }
        int i = 0;

        foreach (var item in inventoryDictionary[typeof(ArmorStats)])
        {
            itemsShowedList[i].item = item;
            itemsShowedList[i].image.sprite = item.itemSprite;
            i++;
        }
       /* foreach (ArmorStats armorStats in armorList)
        {
            itemsShowedList[i].item = armorStats;
            itemsShowedList[i].image.sprite = armorStats.itemSprite;
            i++;
        }*/
    }

    /*public void DisplayConsumables()
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].gameObject.tag == "Potion")
            {
                PotionBehaviour currentPotion = itemList[i].gameObject.GetComponent<PotionBehaviour>();
                itemsShowedList[0].image.sprite = currentPotion.UIIcon;
            }
        }
    }*/

    public void DisplayGear()
    {
        foreach (ItemSlotBehaviour itemSlot in itemsShowedList)
        {
            itemSlot.image.sprite = defaultSprite;
            itemSlot.item = null;
        }
        int i = 0;

        foreach (var itemList in inventoryDictionary)
        {
            foreach(var item in itemList.Value)
            {
                itemsShowedList[i].item = item;
                itemsShowedList[i].image.sprite = item.itemSprite;
                i++;
            }
        }
        /*foreach (ItemStats itemStats in itemList)
        {
            itemsShowedList[i].item = itemStats;
            itemsShowedList[i].image.sprite = itemStats.itemSprite;
            i++;
        }*/
    }

    public void DisplayEquippedWeapon(WeaponStats equippedWeapon)
    {
        switch (equippedWeapon.weaponType)
        {
            case WeaponStats.WeaponType.Slashing:
                sword = equippedWeapon;
                activeSword.sprite = equippedWeapon.itemSprite;
                break;
            case WeaponStats.WeaponType.Piercing:
                bow = equippedWeapon;
                activeBow.sprite = equippedWeapon.itemSprite;
                break;
            case WeaponStats.WeaponType.Bludgeoning:
                hammer = equippedWeapon;
                activeHammer.sprite = equippedWeapon.itemSprite;
                break;
            case WeaponStats.WeaponType.Bleeding:
                dagger = equippedWeapon;
                activeDagger.sprite = equippedWeapon.itemSprite;
                break;
            case WeaponStats.WeaponType.Special:
                break;
        }
    }

    public void DisplayEquippedArmor(ArmorStats equippedArmor)
    {
        switch (equippedArmor.armorType)
        {
            case ArmorStats.ArmorType.Helm:
                helm = equippedArmor;
                activeHelm.sprite = equippedArmor.itemSprite;
                break;
            case ArmorStats.ArmorType.Chest:
                chest = equippedArmor;
                activeChest.sprite = equippedArmor.itemSprite;
                break;
            case ArmorStats.ArmorType.Legs:
                legs = equippedArmor;
                activeLegs.sprite = equippedArmor.itemSprite;
                break;
            case ArmorStats.ArmorType.Boots:
                boots = equippedArmor;
                activeBoots.sprite = equippedArmor.itemSprite;
                break;
          
        }
    }
}
