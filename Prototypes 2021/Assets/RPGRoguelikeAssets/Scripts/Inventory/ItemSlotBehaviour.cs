﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class ItemSlotBehaviour : MonoBehaviour, IPointerEnterHandler,IPointerDownHandler
{
	public static Action<ItemStats> EquipItem;

	public ItemStats item;
	public Image image;

	public TextMeshProUGUI toolTip;
	private void Awake()
	{
		image = GetComponent<Image>();
	}
	public void OnPointerEnter(PointerEventData eventData)
	{
		toolTip.gameObject.SetActive(true);
		toolTip.text = item.name;
	}

    public void OnPointerDown(PointerEventData eventData)
    {
		EquipItem?.Invoke(item);
	}

}
