﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Collectible/Armor", fileName = "Armor")]
public class ArmorStats : ItemStats
{
    public enum ArmorType
    {
        Helm, Chest, Legs, Boots
    }

    public ArmorType armorType;
    public int armor;
}
