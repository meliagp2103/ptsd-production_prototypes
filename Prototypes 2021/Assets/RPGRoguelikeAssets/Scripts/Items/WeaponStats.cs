﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Collectible/Weapon", fileName = "Weapon")]
public class WeaponStats : ItemStats
{
    public enum WeaponType
    {
        Slashing, Piercing, Bludgeoning, Bleeding, Special
    }

    public WeaponType weaponType;
    public float damage;
}
