﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class QTEManager : MonoBehaviour
{
    public GameObject PlayerTurnUI;
    public GameObject EnemyTurnUI;
    public PlayerButtons test;

    public enum battleState { playerTurn, enemyTurn }
    public battleState currentState;
    public static QTEManager instance;
    public GameObject pointToHitPrefab;
    public List<GameObject> pointsToHit = new List<GameObject>();
    public List<GameObject> numbers = new List<GameObject>();
    public int numberPointsToSpawn;

    public int currentWeapon;
    public int damage;
    public int damageTaken;


    [Header("ButtonPress")]
    public Image ButtonPress;
    public float fillValue;
    public float decreaseValue;

    [Header("Dodge")]
    public Image attackButton;
    public float dodgeDuration;
    private float dodgetimer;
    public AnimationCurve dodgeCurve;

    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()  // When pressing attack button
    {
        currentWeapon = test.currentWeapon;
        switch (currentState)
        {
            case battleState.enemyTurn:
                attackButton.gameObject.SetActive(true);
                return;

            case battleState.playerTurn:
                switch (currentWeapon)
                {
                    case 0:
                        for (int i = 0; i < numberPointsToSpawn; i++)
                        {
                            GameObject point = Instantiate(numbers[i], transform.position, Quaternion.identity, transform);
                            RectTransform pointTransform = point.GetComponent<RectTransform>();
                            int xPos = Random.Range(100, 1400);
                            int yPos = Random.Range(0, -800);
                            pointTransform.anchoredPosition = new Vector2(xPos, yPos);
                            pointsToHit.Add(point);
                        }
                        return;

                    case 1:
                        ButtonPress.gameObject.SetActive(true);
                        ButtonPress.fillAmount = 0;
                        return;
                }
                return;
        }
    }

    private void Update()
    {
        if (attackButton.gameObject.activeSelf == true )
        {
            dodgetimer += Time.deltaTime;
            attackButton.fillAmount = dodgetimer / dodgeDuration;
            attackButton.color = Color.Lerp(Color.red, Color.green, dodgeCurve.Evaluate(dodgetimer/dodgeDuration));
        }

        if(ButtonPress.gameObject.activeSelf == true)
        {
            ButtonPress.fillAmount -= decreaseValue * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                ButtonPress.fillAmount += fillValue;
            }
        }
    }
    private void OnDisable()
    {
        switch (currentState)
        {
            case battleState.enemyTurn:
                currentState = battleState.playerTurn;
                PlayerTurnUI.SetActive(true);
                EnemyTurnUI.SetActive(false);
                if (GameManager.instance.currentEnemy != null)
                {
                    GameManager.instance.currentEnemy.health -= damage;

                }
                if (GameManager.instance.currentEnemy.health > 0)
                {
                    GameManager.instance.health -= damageTaken;
                }
                dodgetimer = 0;
                attackButton.gameObject.SetActive(false);
                damage = 0;
                return;


            case battleState.playerTurn:
                currentState = battleState.enemyTurn;
                EnemyTurnUI.SetActive(true);
                PlayerTurnUI.SetActive(false);
                switch (currentWeapon)
                {
                    case 0:

                        if (GameManager.instance.currentEnemy != null)
                        {
                            damage = 10 - pointsToHit.Count * 2;
                            GameManager.instance.currentEnemy.health -= damage;                       
                        }
                      
                        foreach (GameObject point in pointsToHit)
                        {
                            Destroy(point.gameObject);
                        }
                        pointsToHit.Clear();
                        damage = 0;

                        return;
                    case 1:
                        damage = (int)(10 * ButtonPress.fillAmount);
                        if (GameManager.instance.currentEnemy != null)
                        {
                            GameManager.instance.currentEnemy.health -= damage;

                        }                        
                        ButtonPress.gameObject.SetActive(false);
                        damage = 0;
                        return;
                }
                return;
        }
    }
    public void chargeAttackButton()
    {
        int damageReduction = (int)(attackButton.color.g * 5);
        damageTaken = 5 - damageReduction;
        dodgetimer = dodgeDuration;
        GameManager.instance.timer += GameManager.instance.quickTimeEventTime;
    }
}

