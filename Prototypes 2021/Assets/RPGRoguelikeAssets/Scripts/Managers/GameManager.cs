﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UIElements;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public enum gameState { mapNavigation, combat, quickTimeEvent}
    public gameState currentGameState;
        
    public GameObject combatPanel;
    public GameObject inventoryButton;
    public GameObject quickTimeEventPanel;

    public Camera mainCamera;
    public Camera combatCamera;

    public Transform combatEnemyPosition;
    public combatEnemy currentEnemy;

    public float quickTimeEventTime;
    public float timer;

    public GameObject swithWeapon;

    public int health;
    public TextMeshProUGUI playerHP;
    public TextMeshProUGUI enemyHP;
    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        switch (currentGameState)
        {
            case gameState.mapNavigation:
                return;
            case gameState.combat:
                playerHP.text = health.ToString();
                enemyHP.text = currentEnemy.health.ToString();
                return;
            case gameState.quickTimeEvent:
                timer += Time.deltaTime;
                if(timer >= quickTimeEventTime)
                {
                    quickTimeEventPanel.SetActive(false);
                    timer = 0;
                    if(currentEnemy.health > 0)
                    {
                        ChangeGameState(gameState.combat);
                    }
                    else
                    {
                        ChangeGameState(gameState.mapNavigation);
                        Destroy(currentEnemy.gameObject);
                    }
                }
                return;
        }
    }

    public void ChangeGameState(gameState newGameState)
    {
        currentGameState = newGameState;
        switch (currentGameState)
        {
            case gameState.combat:
                combatCamera.gameObject.SetActive(true);
                mainCamera.gameObject.SetActive(false);
                inventoryButton.SetActive(false);
                combatPanel.SetActive(true);
                return;
            case gameState.mapNavigation:
                combatCamera.gameObject.SetActive(false);
                mainCamera.gameObject.SetActive(true);
                inventoryButton.SetActive(true);
                combatPanel.SetActive(false);
                return;
            case gameState.quickTimeEvent:
                quickTimeEventPanel.SetActive(true);
                return;
        }
    }

    public void ChangeWeapon()
    {
        if(QTEManager.instance.currentWeapon == 1)
        {
            QTEManager.instance.currentWeapon = 0;
        }
        else
        {
            QTEManager.instance.currentWeapon = 1;
        }
    }
}
